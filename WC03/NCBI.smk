import os
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="b.berkhout@st.hanze.nl")

query = '"Zika virus"[Organism] AND (("9000"[SLEN] : "20000"[SLEN]) AND ("2017/03/20"[PDAT] : "2017/03/24"[PDAT])) '
accessions = NCBI.search(query, retmax=4)

input_files = expand("{acc}.fasta", acc=accessions)

rule all:
    input:
        "multi.fasta"



rule download_and_combine:
    input:
        NCBI.remote(input_files, db="nuccore", seq_start=5000)
    output:
        "multi.fasta"
    benchmark:
        "benchmarks/benchmark.txt"
    threads: 8
    run:
        outputName = os.path.basename("test.fasta")
        shell("cat {threads} {input} > multi.fasta")

