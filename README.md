# README #

To get the final assignment repository setup, download the code and create a config file like 'config.yaml'.  
Then, in the command line run `snakemake --cores N` with N being the desired number of cores to use during the analysis.  
And thats all you need!

### What is this repository for? ###

This repository contains the weekly assignments as well as the final assignment for the Dataprocessing course.
The final assignment is a sQTL colocalization pipeline in which sQTL files and variant data for a GWAS trait can be inputted. 
The program creates the necessary input files for the colocalization step and then performs colocalization.
The results are then merged and the colocalized genes are plotted using a scatterplot.

### How do I get set up? ###
The following languages are requiered:

* Python (V.)  
* R (V.) 

This pipeline requires the following python packages/libraries:  

* Seaborn  
* matplotlib  
* pandas  
* snakemake  

and the following R package is required:

* coloc
