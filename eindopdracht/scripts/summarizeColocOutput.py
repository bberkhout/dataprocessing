import glob
import sys
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import gzip


def make_dict_from_file(file, reverse=False):
    dict = {}
    with open(file, 'r') as f:
        for line in f:
            if reverse:
                val, key = line.strip().split('\t')
            else:
                key, val = line.strip().split('\t')
            dict[key] = val
    f.close()
    return dict


input_dir = sys.argv[1]
output_dir = sys.argv[2]
ens_to_gene = make_dict_from_file(sys.argv[3])

coloc_files = glob.glob(f'*-merged.txt')

output = {}
trait = os.path.basename(coloc_files[0]).split('-')[0]
for file in coloc_files:
    print(file)
    trait = os.path.basename(file).split('-')[0]

    splice_type = os.path.basename(file).split('-')[1]
    if splice_type not in output.keys():
        output[splice_type] = []

    with open(file, 'r') as f:
        for line in f:
            if line.startswith('eventID'):
                continue
            elems = line.strip().split('\t')

            if float(elems[-1]) > 0.8:
                gene = elems[0].split('|')[0].strip()
                output[splice_type] = [ens_to_gene[gene], float(elems[-1])]
    f.close()

coloc_genes = pd.DataFrame.from_dict(output, orient='index', columns=['gene', 'PP4'])
coloc_genes['splicetype'] = list(coloc_genes.index)
p1 = sns.scatterplot('splicetype', 'PP4', data=coloc_genes, size=8, legend=False)
for i in range(coloc_genes.shape[0]):
    plt.text(coloc_genes.splicetype[i], coloc_genes.PP4[i],
             coloc_genes.gene[i], horizontalalignment='left',
             size='medium', color='black', weight='semibold')

plt.savefig(f'{trait}-colocalized-genes.pdf')
print(output)