
args = commandArgs(trailingOnly=TRUE)
if (length(args)<4) {
  stop("At least four arguments must be supplied: inputdir eqtlsamples cases controls", call.=FALSE)
} 

library(coloc)


inputdir=args[1]
outputdir=args[2]
eqtlsamples=as.integer(args[3])
cases=as.integer(args[4])
controls=as.integer(args[5])
gwas_type=args[6]

print(file.path(outputdir))
dir.create(file.path(outputdir))
#n1<-3780 

# MS
#s2<-14802
#n2<-26703+s2
#s2 = s2/n2

# ALS
#s2<-2579
#n2<-2767+s2

# ALZ
#s2<-24087
#n2<-47793+s2

# PKD
#s2<-20184
#n2<-397324+s2

s2 = cases/controls

# inputdir="D:\\Sync\\SyncThing\\Postdoc2\\2019-BioGen\\data\\2019-04-Freeze2\\2019-07-29-GWAS\\b38\\eqtlmerge\\ALZ\\"

# get a list of files in the inputdir
files <- list.files(path=inputdir, pattern="*.txt.gz", full.names=TRUE, recursive=FALSE)

# iterate over genes for disease in folder inputfolder
lapply(files,function(filename) {

  if(!endsWith(filename, "coloc.gz") && !endsWith(filename,"colocsummary.gz")){
  
  filebasename <- gsub("^.*/", "", filename) 
	fileout=paste(outputdir,filebasename,"-coloc.gz",sep="")
	fileoutsummary=paste(outputdir,filebasename,"-colocsummary.gz",sep="")
	if(file.exists(fileoutsummary)){
	  file.remove(fileoutsummary)  
	}
	print(paste("Reading",filename))
	data<-read.table(gzfile(filename),header=TRUE,sep="\t")
	data <- data[complete.cases(data),]

	
	beta1<-data$beta1
	varbeta1<-data$se1*data$se1
	beta2<-data$beta2
	varbeta2<-data$se2*data$se2
	eqtlsamples <- data$eqtlsamples

	
	maf<-data$maf
	  
  if (gwas_type == 'cc'){
    myres <- coloc.abf(dataset1=list(beta=beta1, varbeta=varbeta1, N=eqtlsamples,type="quant", snp=data$SNP),
                       dataset2=list(beta=beta2, varbeta=varbeta2, N=cases+controls,s=s2, type=gwas_type, snp=data$SNP),
                       MAF=maf)
  }
	else{
	  myres <- coloc.abf(dataset1=list(beta=beta1, varbeta=varbeta1, N=eqtlsamples,type="quant", snp=data$SNP),
	                     dataset2=list(beta=beta2, varbeta=varbeta2, N=cases,type=gwas_type, snp=data$SNP),
	                     MAF=maf)
	}
		
		
	write.table(myres$results,file=gzfile(fileout),quote=FALSE,sep="\t")
	write.table(myres$summary,file=gzfile(fileoutsummary),quote=FALSE,sep="\t")  
  }
  
})
