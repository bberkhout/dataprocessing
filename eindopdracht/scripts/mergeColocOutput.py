import sys
import gzip
import glob
import os


def openfile(filename):
    if filename.endswith('.gz'):
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')


if len(sys.argv) != 5:
    print('Usage: input_dir\toutput_dir\tsplicetype\tdisease')
    sys.exit(0)
    
input_dir = sys.argv[1]
output_dir = sys.argv[2]
splicetype = sys.argv[3]
disease = sys.argv[4]

coloc_output = glob.glob(f'{input_dir}/*-colocsummary.gz')

merge_dict = {}
for file in coloc_output:
    file_event = os.path.basename(file).split('-coloc-input')[0]
    event_id = file_event.replace('-', '|')
    with openfile(file) as f:
        values = []
        for line in f:
            if not line.startswith('PP'):
                continue
            p_val = line.strip().split('\t')[1]
            values.append(p_val)
        merge_dict[event_id] = values


outfile = open(f'{output_dir}/{disease}-{splicetype}-colocsummary-merged.txt', 'w')
outfile.write('eventID\tPP.H0\tPP.H1\tPP.H2\tPP.H3\tPP.H4')
for event in merge_dict.keys():
    vals = '\t'.join(merge_dict[event])
    outfile.write(f'\n{event}\t{vals}')
outfile.close()
