args = commandArgs(trailingOnly=TRUE)
if (length(args)<4) {
  stop("At least four arguments must be supplied: inputdir eqtlsamples cases controls", call.=FALSE)
} 

library(coloc)

inputfile=args[1]
outputdir=args[2]
eqtlsamples=as.integer(args[3])
cases=as.integer(args[4])
controls=as.integer(args[5])
gwas_type=args[6]

filebasename <- gsub("^.*/", "", inputfile) 
fileout=paste(filebasename,"-coloc.gz",sep="")
fileoutsummary=paste(filebasename,"-colocsummary.gz",sep="")
if(file.exists(fileoutsummary)){
  file.remove(fileoutsummary)  
}

data<-read.table(gzfile(inputfile),header=TRUE,sep="\t")
data <- data[complete.cases(data),]


beta1<-data$beta1
varbeta1<-data$se1*data$se1
beta2<-data$beta2
varbeta2<-data$se2*data$se2
eqtlsamples <- data$eqtlsamples


maf<-data$maf

if (gwas_type == 'cc'){
  myres <- coloc.abf(dataset1=list(beta=beta1, varbeta=varbeta1, N=eqtlsamples,type="quant", snp=data$SNP),
                     dataset2=list(beta=beta2, varbeta=varbeta2, N=cases+controls,s=s2, type=gwas_type, snp=data$SNP),
                     MAF=maf)
}else {
  myres <- coloc.abf(dataset1=list(beta=beta1, varbeta=varbeta1, N=eqtlsamples,type="quant", snp=data$SNP),
                     dataset2=list(beta=beta2, varbeta=varbeta2, N=cases,type=gwas_type, snp=data$SNP),
                     MAF=maf)
}


write.table(myres$results,file=gzfile(fileout),quote=FALSE,sep="\t")
write.table(myres$summary,file=gzfile(fileoutsummary),quote=FALSE,sep="\t") 