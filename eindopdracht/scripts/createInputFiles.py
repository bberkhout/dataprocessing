import gzip
import sys
import argparse
import os


def openfile(filename):
    if filename.endswith('.gz'):
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')


def create_sQTL_file(sqtls, beta_col, se_col, snp_col, n_col):
    print('Getting data from sQTL_file...')
    sqtl_data = {}
    snp_ids = []
    col_positions = {}
    beta_pos = 0
    se_pos = 0
    snp_pos = 0
    n_pos = 0
    id_pos = 0
    with openfile(sqtls) as f:
        for line in f:
            if line.strip().startswith('spliceEventID') or line.strip().startswith('Unnamed: 0'):
                header = line.split('\t')
                for i, col in enumerate(header):
                    if col == beta_col:
                        beta_pos = i
                    elif col == se_col:
                        se_pos = i
                    elif col == snp_col:
                        snp_pos = i
                    elif col == n_col:
                        n_pos = i
                    elif 'ID' in col:
                        id_pos = i
                        
                continue
            line = line.strip()

            elems = line.split('\t')
            splice_id = elems[id_pos].strip('"')
            pval = float(elems[-1])
            if pval <= 0.05:
                if splice_id not in sqtl_data.keys():
                    sqtl_data[splice_id] = []

            if splice_id in sqtl_data.keys():
                rsid = elems[snp_pos].split(':')[2]
                snp_ids.append(rsid)
                sqtl_data[splice_id].append([elems[snp_pos], elems[se_pos], elems[beta_pos], elems[n_pos]])
    return sqtl_data, snp_ids


def read_gwas_file(gwas, sqtl_data, snps, beta_col, se_col, snp_col):
    print('Getting data from GWAS file')
    snps = set(snps)
    gwas_snps = {}
    beta_pos = 0
    se_pos = 0
    snp_pos = 0
    with openfile(gwas) as f:
        first_line = True
        for line in f:
            if first_line:
                header = line.split('\t')
                for i, col in enumerate(header):
                    if col == beta_col:
                        beta_pos = i
                    elif col == se_col:
                        se_pos = i
                    elif col == snp_col:
                        snp_pos = i

                first_line = False
                continue
            line = line.strip()
            elems = line.split('\t')
            snpid = elems[snp_pos]
            if snpid in snps:
                gwas_snps[snpid] = [elems[se_pos], elems[beta_pos]]
    return gwas_snps


def add_gwas_data(sqtl_data, gwas_snps, snp_maf):
    print('Adding GWAS data to sQTL data')
    for event in sqtl_data:
        for i in range(len(sqtl_data[event])):
            sqtl_snp = sqtl_data[event][i][0].split(':')[2]
            try:
                gwas_data = gwas_snps[sqtl_snp]
                maf = snp_maf[sqtl_snp]
                for val in gwas_data:
                    sqtl_data[event][i].append(val)
                sqtl_data[event][i].append(maf)
            except KeyError:
                continue
    return sqtl_data


def get_maf(maf_file, snps):
    print('Adding MAF to data...')

    snp_maf = {}
    snps = set(snps)
    with openfile(maf_file) as f:
        for line in f:
            if line.startswith('SNP'):
                continue
            elems = line.strip().split('\t')
            rsid = elems[0].split(':')[2]
            if rsid in snps:
                if elems[-1] != 'NaN':
                    snp_maf[rsid] = elems[-1]
    return snp_maf


def write_output(data, outdir):
    print('Writing input files to ' + outdir)
    non_matched = []
    # Remove splice events with no matched SNP ids
    for event in data.keys():
        no_match = True
        for val in data[event]:
            if len(val) == 7:
                no_match = False

        if no_match is True:
            non_matched.append(event)

    for event in data.keys():
        if event in non_matched:
            continue
        event_id = '-'.join(event.split('|')[0:2])
        # sample_file.write(event_id)

        outfile = open(f'{outdir}/{event_id}-coloc-input.txt.gz', 'w')
        outfile.write('SNP\tse1\tbeta1\teqtlsamples\tse2\tbeta2\tmaf\n')

        for val in data[event]:
            if len(val) != 7:
                continue
            outfile.write('\t'.join(val))
            outfile.write('\n')

        outfile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_sqtl', help='input sQTL file in txt or .gz format')
    parser.add_argument('input_gwas', help='GWAS file')
    parser.add_argument('outdir')
    parser.add_argument('MAF_data')
    parser.add_argument('gwas_beta', help='GWAS beta col name')
    parser.add_argument('gwas_se', help='GWAS s.e. col name')
    parser.add_argument('gwas_snp', help='GWAS snp col name')
    parser.add_argument('sqtl_beta', help='sQTL beta col name')
    parser.add_argument('sqtl_se', help='sQTL se col name')
    parser.add_argument('sqtl_snp', help='sQTL SNP col name')
    parser.add_argument('sqtl_nsamples', help='n-samples col name')
    args = parser.parse_args()

    splicetype = args.input_sqtl.split('/')[-1].split('-')[0]
    trait = args.outdir.split('/')[-1]
    if not os.path.exists(f'{args.outdir}/{splicetype}'):
        os.makedirs(f'{args.outdir}/{splicetype}')

    outdir = f'{args.outdir}/{splicetype}'
    args = parser.parse_args()
    sqtl_snps, event_snp = create_sQTL_file(args.input_sqtl, args.sqtl_beta, args.sqtl_se, args.sqtl_snp, args.sqtl_nsamples)
    gwas_data = read_gwas_file(args.input_gwas, sqtl_snps, event_snp, args.gwas_beta, args.gwas_se, args.gwas_snp)
    maf_data = get_maf(args.MAF_data, event_snp)
    merged_snps = add_gwas_data(sqtl_snps, gwas_data, maf_data)

    write_output(merged_snps, outdir)
    os.system(f'touch {outdir}/{trait}-{splicetype}.done')
    print('done')
